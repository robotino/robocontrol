package com.jrussostudio.robocontrol.communications

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import android.os.ParcelUuid
import android.util.Log
import java.io.IOException
import java.util.*

class BluetoothConnectionManager(private val robotinoBluetoothAdapter: BluetoothAdapter,
                                 listener: ConnectionListener): Runnable {
    private val LOG_TAG: String = "BTConnectManager"

    private val btUid: ParcelUuid = ParcelUuid(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"))

    private var robotinoDeviceDiscovered: Boolean = false

    private var listener: ConnectionListener? = listener
//    private val robotinoBluetoothAdapter: BluetoothAdapter? = robotinoBluetoothAdapter
    private var robotinoBluetoothDevice: BluetoothDevice? = null
    private var robotinoBluetoothSocket: BluetoothSocket? = null

    interface ConnectionListener {
        fun onManageConnection(bluetoothSocket: BluetoothSocket)
    }

    override fun run() {
        val pairedDevices: Set<BluetoothDevice>? = robotinoBluetoothAdapter.bondedDevices

        pairedDevices?.forEach { device ->
            val deviceName = device.name
            val deviceAddress = device.address

            Log.v(LOG_TAG, "Found device named $deviceName at address $deviceAddress")

            if (deviceName == "HC-06") {
                robotinoDeviceDiscovered = true

                // Connect bluetooth in a background thread
                robotinoBluetoothDevice = robotinoBluetoothAdapter.getRemoteDevice(deviceAddress)

                robotinoBluetoothSocket = try {
                    Log.i(LOG_TAG, "Established bluetooth socket connection to Robotino.")
                    robotinoBluetoothDevice!!.createInsecureRfcommSocketToServiceRecord(btUid.uuid)
                } catch (e: IOException) {
                    Log.e(LOG_TAG, "Error establishing bluetooth connection.", e)
                    null
                }

                robotinoBluetoothAdapter.cancelDiscovery()

                try {
                    robotinoBluetoothSocket?.connect()
                    listener?.onManageConnection(robotinoBluetoothSocket!!)
                } catch (e: IOException) {
                    Log.e(LOG_TAG, "Error connecting to bluetooth", e)

                    // TODO: Popup or toaster to indicate error connecting and maybe try again?
                }
            }
        }

        if (!robotinoDeviceDiscovered) {
            Log.e(LOG_TAG,"Could not discover Robotino")
        }
    }

    fun closeBluetoothConnection() {
        try {
            robotinoBluetoothSocket?.close()
        } catch (e: IOException) {
            Log.e(LOG_TAG, "Error closing bluetooth socket.")
        }
    }
}