package com.jrussostudio.robocontrol.communications

import android.bluetooth.BluetoothSocket
import android.os.Handler
import android.os.Message
import android.util.Log
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import android.os.Bundle
import java.nio.charset.Charset

class BluetoothCommunicationsManager(private val bluetoothSocket: BluetoothSocket,
                                     private val handler: Handler): Runnable {
    private val LOG_TAG: String = "BTCommManager"
    private val BT_MSG: String = "BT_MESSAGE"

    private val MESSAGE_READ: Int = 0x00
    private val MESSAGE_WRITE: Int = 0x01

    private val inputStream: InputStream = bluetoothSocket.inputStream
    private val outputStream: OutputStream = bluetoothSocket.outputStream

    private val streamBuffer: ByteArray = ByteArray(256)

    private var keepReading = true

    override fun run() {
        var numBytes: Int
        var readMsg: Message?
        var msgTxt: String?

        keepReading = true

        while (keepReading) {
            try {
                numBytes = inputStream.read(streamBuffer, 0, streamBuffer.size)
                msgTxt = String(streamBuffer).substring(0, numBytes)
                Log.v(LOG_TAG, "Read data from bt: $msgTxt")

                readMsg = handler.obtainMessage(MESSAGE_READ, msgTxt)
                readMsg.sendToTarget()
            } catch (e: IOException) {
                keepReading = false
                Log.e(LOG_TAG, "Error receiving data.", e)
                stopListening()
            }
        }
    }

    fun write(bytes: ByteArray) {
        try {
            outputStream.write(bytes)
        } catch (e: IOException) {
            Log.e(LOG_TAG, "Error sending data.", e)

            // TODO: Notify client UI of error.
        }
    }

    fun stopListening() {
        keepReading = false
        inputStream.close()
        outputStream.close()
        bluetoothSocket.close()
    }
}