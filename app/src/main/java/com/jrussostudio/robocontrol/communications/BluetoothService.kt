package com.jrussostudio.robocontrol.communications

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothSocket
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import com.jrussostudio.robocontrol.exceptions.BluetoothDisabledException
import com.jrussostudio.robocontrol.exceptions.BluetoothUnsupportedException

class BluetoothService @Throws(BluetoothUnsupportedException::class) constructor(listener: BluetoothServiceListener) :
        BluetoothConnectionManager.ConnectionListener {
    private val LOG_TAG: String = "BTService"

    private var btAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
    private var btConnectManager: BluetoothConnectionManager? = null
    private var btCommManager: BluetoothCommunicationsManager? = null

    private var msgReceiver:MsgReceiver? = MsgReceiver()
    private val msgHandler: Handler = Handler(msgReceiver)

    private var listener: BluetoothServiceListener? = listener

    interface BluetoothServiceListener {
        fun onMessageReceived(msg: String)
    }

    inner class MsgReceiver : Handler.Callback {
        override fun handleMessage(msg: Message?): Boolean {
            val msgStr = msg?.obj as String
            Log.v(LOG_TAG, "Received message from bluetooth device: $msgStr")

            listener?.onMessageReceived(msgStr)
            return true
        }
    }

    init {
        var thread: Thread

        if (btAdapter == null)
            throw BluetoothUnsupportedException("No bluetooth adapter found on system.")

        if (btAdapter?.isEnabled == false)
            throw BluetoothDisabledException("Bluetooth adapter is disabled")

        btConnectManager = BluetoothConnectionManager(btAdapter!!, this)
        thread = Thread(btConnectManager)
        thread.start()
    }

    override fun onManageConnection(bluetoothSocket: BluetoothSocket) {
        Log.v(LOG_TAG, "Called onManageConnection")

        btCommManager = BluetoothCommunicationsManager(bluetoothSocket, msgHandler)
        beginListening()
    }

    fun sendMessage(message: String) {
        btCommManager?.write(message.toByteArray())
    }

    fun beginListening() {
        var thread: Thread

        thread = Thread(btCommManager)
        thread.start()
        Log.d(LOG_TAG, "Starting bluetooth connection")
    }

    fun stopListening() {
        btCommManager?.stopListening()
        Log.d(LOG_TAG, "Ceased listening to bluetooth connection")
    }

    fun closeConnection() {
        btCommManager?.stopListening()
        btConnectManager?.closeBluetoothConnection()
        Log.d(LOG_TAG, "Closing bluetooth connection")
    }
}