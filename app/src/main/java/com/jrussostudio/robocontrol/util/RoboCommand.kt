package com.jrussostudio.robocontrol.util

enum class RoboCommand(val code: Char) {
    AUTO        ('a'),
    STOP        ('s'),
    FORWARD     ('f'),
    BACKWARD    ('b'),
    LEFT        ('l'),
    RIGHT       ('r');
}