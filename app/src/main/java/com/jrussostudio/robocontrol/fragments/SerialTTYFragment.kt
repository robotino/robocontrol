package com.jrussostudio.robocontrol.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jrussostudio.robocontrol.R
import kotlinx.android.synthetic.main.fragment_serial_tty.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SerialTTYFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SerialTTYFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class SerialTTYFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null

    private var displayTxtLen: Int = 0
    private var displayMaxLen: Int = 200

    private var ttyTextView: TextView? = null
    private var displayBuffer: StringBuffer = StringBuffer(displayMaxLen)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ttyTextView = activity?.findViewById<TextView>(R.id.ttyOutput)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_serial_tty, container, false)
    }

    fun displayMessage(msg: String) {
        displayTxtLen += msg.length

        if (displayTxtLen >= displayMaxLen) {
            displayBuffer.replace(0, displayBuffer.length - msg.length - 1,
                displayBuffer.substring(msg.length, displayBuffer.length - 1)).append(msg)
        } else {
            displayBuffer.append(msg)
        }

        ttyOutput?.setText(displayBuffer)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment SerialTTYFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            SerialTTYFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}
