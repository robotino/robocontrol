package com.jrussostudio.robocontrol

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.jrussostudio.robocontrol.BuildConfig.IGNORE_BLUETOOTH
import com.jrussostudio.robocontrol.communications.BluetoothService
import com.jrussostudio.robocontrol.exceptions.BluetoothDisabledException
import com.jrussostudio.robocontrol.exceptions.BluetoothUnsupportedException
import com.jrussostudio.robocontrol.fragments.ControlPadFragment
import com.jrussostudio.robocontrol.fragments.SerialTTYFragment
import com.jrussostudio.robocontrol.util.RoboCommand
import kotlinx.android.synthetic.main.fragment_control_pad.*
import kotlin.system.exitProcess

class MainActivity : AppCompatActivity() , SerialTTYFragment.OnFragmentInteractionListener,
    ControlPadFragment.OnFragmentInteractionListener, BluetoothService.BluetoothServiceListener {

    private val LOG_TAG: String = "MainActivity"

    private var serialMon: SerialTTYFragment? = null
    private var controlPad: ControlPadFragment? = null
    private var currentCommand: RoboCommand = RoboCommand.STOP
    private var REQUEST_ENABLE_BT: Int = 0x00000001

    private var bluetoothService: BluetoothService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        controlPad = supportFragmentManager.findFragmentById(R.id.controlSection) as ControlPadFragment?
        serialMon = supportFragmentManager.findFragmentById(R.id.ttySection) as SerialTTYFragment?

        bluetoothService = try {
            BluetoothService(this)
        } catch (e: BluetoothUnsupportedException) {
            if (!IGNORE_BLUETOOTH) {
                exitProcess(0)
            }
            null
        } catch (e: BluetoothDisabledException) {
            val enableBTIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBTIntent, REQUEST_ENABLE_BT)
            null
        }
    }

    override fun onPostResume() {
        super.onPostResume()
//        bluetoothService?.beginListening()
    }

    override fun onStart() {
        super.onStart()
        bluetoothService?.beginListening()
    }

    override fun onStop() {
        super.onStop()
        bluetoothService?.stopListening()
    }

    override fun onDestroy() {
        super.onDestroy()
        bluetoothService?.closeConnection()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_CANCELED) {
                // System did not grant access to bluetooth

                if (!IGNORE_BLUETOOTH) {
                    exitProcess(0)
                }
            } else if (resultCode == Activity.RESULT_OK) {
                bluetoothService = try {
                    BluetoothService(this)
                } catch (e: Exception) {
                    if (!IGNORE_BLUETOOTH) {
                        exitProcess(0)
                    }
                    null
                }
            }
        }
    }

    override fun onMessageReceived(msg: String) {
        serialMon?.displayMessage(msg)
    }

    fun sendMove(button: View) {
        when (button) {
            ControlStop -> onUpdateCommand(RoboCommand.STOP)
            ControlUp -> onUpdateCommand(RoboCommand.FORWARD)
            ControlDown -> onUpdateCommand(RoboCommand.BACKWARD)
            ControlLeft -> onUpdateCommand(RoboCommand.LEFT)
            ControlRight -> onUpdateCommand(RoboCommand.RIGHT)
        }
    }

    override fun onUpdateCommand(command: RoboCommand) {
        if (command != currentCommand) {
            currentCommand = command
        } else if (command == RoboCommand.STOP) {
            currentCommand =
                if (currentCommand == RoboCommand.AUTO) RoboCommand.STOP
                else RoboCommand.AUTO
        }

        emitMessage(currentCommand.code)
    }

    private fun emitMessage(command: Char) {
        Log.d(LOG_TAG, "Issued robot command: $command")

        bluetoothService?.sendMessage(command.toString())
    }
}