package com.jrussostudio.robocontrol.exceptions

class BluetoothDisabledException(message: String?): Exception(message)