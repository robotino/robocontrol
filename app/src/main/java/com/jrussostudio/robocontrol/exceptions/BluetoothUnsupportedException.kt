package com.jrussostudio.robocontrol.exceptions

class BluetoothUnsupportedException(message: String?) : Exception(message)